package pe.beyond.wally.websocket.manager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import pe.beyond.wally.external.printer.wallyprinter.WallyESCPOSPrinter;
import pe.beyond.wally.external.printer.wallyprinter.WallyPrinter;
import pe.beyond.wally.external.printer.wallyprinter.escpos.WallyESCPOSPrinterBIXOLON;
import pe.beyond.wally.external.printer.wallyprinter.escpos.WallyESCPOSPrinterEPSON;
import pe.beyond.wally.external.printer.wallyprinter.escpos.WallyESCPOSPrinterEthernet;
import pe.beyond.wally.external.printer.wallyprinter.escpos.WallyESCPOSPrinterSTARSP742;

public class WallyPrinterManager {

	final ExecutorService es = Executors.newFixedThreadPool(20);
	String baseIP = "192.168.1.0";
	int port = 9100;

	// SCAN PRINTERS
	public static class ScanResult {
		private boolean isOpen = false;
		private String sIP = "";

		// constructor
		ScanResult(String s, int p, boolean b) {
			sIP = s;
			isOpen = b;
		}

		// getters
		public ScanResult get() {
			return this;
		}

		@Override
		public String toString() {
			return sIP;
		}
	}

	boolean isMac;
	boolean isWindows;
	boolean isLinux;

	public WallyPrinterManager() {

		String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
		if ((OS.indexOf("mac") >= 0) || (OS.indexOf("darwin") >= 0)) {
			isMac = true;
		} else if (OS.indexOf("win") >= 0) {
			isWindows = true;
		} else if (OS.indexOf("nux") >= 0) {
			isLinux = true;
		}

	}

	public List<WallyPrinter> getPrinters() throws Exception {
		List<WallyPrinter> printers = new ArrayList<WallyPrinter>();
		final List<Future<ScanResult>> futures = new ArrayList<Future<ScanResult>>();

		String newIp = getIp();
		if (newIp == null)
			newIp = baseIP;

		log("newIp" + newIp);

		String splittedIP = newIp.substring(0, newIp.lastIndexOf("."));

		log("splittedIP:" + splittedIP);

		log("startDiscovery1: starting futures...");

		// start many threads for scanning
		int timeout = 500;

		for (int ip1 = 1; ip1 <= 254; ip1++) {
			String sip = String.format(splittedIP + "." + "" + ip1);
			futures.add(portIsOpen(es, sip, port, timeout));
		}

		log("startDiscovery1: es.shutdown()");
		es.shutdown();
		log("startDiscovery1 getting results...");
		for (final Future<ScanResult> f : futures) {
			try {
				if (f.get().isOpen) {
					log("portScan:" + f.get().sIP + " 9100 open");
					// Send the name of the connected device back to the UI Activity
					printers.add(addWallyPrinter(f.get().sIP));
					log("added host msg for " + f.get().sIP);
				} else {
					// log("portScan:" + f.get().sIP + " 9100 closed");
				}
			} catch (ExecutionException e) {
				log("ExecutionException: " + e.getMessage());
			} catch (InterruptedException e) {
				log("InterruptedException: " + e.getMessage());
			}
		}

		log("startDiscovery1: END");
		return printers;
	}

	public WallyPrinter addWallyPrinter(String ip) throws Exception {
		WallyPrinter printer = new WallyPrinter();
		printer.setIp(ip);
		String macAddress = "";
		try {
			InetAddress.getByName(ip).isReachable(100);
			;
		} catch (Exception e) {
			e.printStackTrace();
		}

		macAddress = getHardwareAddress(printer.getIp(), 1);

		if(macAddress == null)
			throw new Exception("");
		String[] macParts = null;
		if (isMac || isLinux) {
			macParts = macAddress.split(":");
		}
		else if (isWindows) {
			macParts = macAddress.split("-");
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < macParts.length; i++) {
			String string = macParts[i];
			if (string.length() == 1) {
				string = "0" + string;
			}
			sb.append(string);
			if (i < macParts.length - 1) {
				sb.append(":");
			}
		}

		String formattedMacAddress = sb.toString().toUpperCase();
		System.out.println("formattedMacAddress: " + formattedMacAddress);

		printer.setMacAddress(formattedMacAddress);
		printer.setBrand(getWallyESCPOSPrinterBrand(printer.getMacAddress()));
		return printer;
	}

	static Future<ScanResult> portIsOpen(final ExecutorService es, final String ip, final int port, final int timeout) {
		return es.submit(new Callable<ScanResult>() {
			public ScanResult call() {
				try {
					Socket socket = new Socket();
					socket.connect(new InetSocketAddress(ip, port), timeout);
					socket.setKeepAlive(false);
					socket.setReuseAddress(false);
					socket.setSoTimeout(timeout);
					socket.close();
					return new ScanResult(ip, port, true);// true;
				} catch (Exception ex) {
					// ex.printStackTrace();
					return new ScanResult(ip, port, false);// false;
				}
			}
		});
	}

	/*
	 * public static void main(String[] args) {
	 * 
	 * String ip = getIp();
	 * 
	 * System.out.println("IPPPPP: " + getIp());
	 * 
	 * 
	 * System.out.println("IP LOCAL: " + ip); System.out.println("MAC AQUI: " +
	 * getHardwareAddress(ip,1)); WallyPrinterManager manager = new
	 * WallyPrinterManager(); manager.getPrinters();
	 * 
	 * }
	 */

	public static String getIp() {
		String ipAddress = null;
		Enumeration<NetworkInterface> net = null;
		try {
			net = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			throw new RuntimeException(e);
		}

		while (net.hasMoreElements()) {
			NetworkInterface element = net.nextElement();
			Enumeration<InetAddress> addresses = element.getInetAddresses();
			while (addresses.hasMoreElements()) {
				InetAddress ip = addresses.nextElement();
				if (ip instanceof Inet4Address) {

					if (ip.isSiteLocalAddress()) {

						ipAddress = ip.getHostAddress();
					}

				}

			}
		}
		return ipAddress;
	}
	/*
	 * ANDROID line:IP address HW type Flags HW address Mask Device
	 * line:192.168.56.1 0x1 0x2 0a:00:27:00:00:00 * eth0
	 * 
	 * MAC ? (192.168.1.15) at 94:b1:a:1d:5a:a3 on en0 ifscope [ethernet]
	 * 
	 * LINUX Address HWtype HWaddress Flags Mask Iface 192.168.1.150 ether
	 * 00:15:94:51:28:ca C enp8s0
	 * 
	 * Windows 192.168.1.150 00-00-00-00-00-00 static
	 * 
	 */

	// private final static String MAC_RE =
	// "^%s\\s+0x1\\s+0x2\\s+([:0-9a-fA-F]+)\\s+\\*\\s+\\w+$";

	private final static int BUF = 8 * 1024;

	public String getHardwareAddress(String ip, final int count) {
		String hw = null;
		if (count < 0) {
			return hw;
		}

		BufferedReader bufferedReader = null;
		try {
			log("IP BUSCANDO: " + ip);
			if (ip != null) {

				Runtime rt = Runtime.getRuntime();
				String[] commands = null;
				if (isMac || isLinux)
					commands = new String[] { "arp", ip };
				else if (isWindows)
					commands = new String[] { "arp", "-a" };

				Process proc = rt.exec(commands);
				bufferedReader = new BufferedReader(new InputStreamReader(proc.getInputStream()));
				String line;
				while ((line = bufferedReader.readLine()) != null) {
					log("line:" + line);
					if (line.contains(ip)) {
						// si es mac
						if (isMac) {
							if(line.contains("no entry"))
								hw = "(incomplete)";
							else
								hw = ((line.split("at")[1]).split("on")[0]).trim();
						}
						else if (isWindows) {
							System.out.println("IS WINDOWS:");
							String data = line.replaceAll("\\s+", " ");
							System.out.println(data);
							hw = data.split(" ")[2];
						} else if (isLinux) {
							System.out.println("IS LINUX:");
							String data = line.replaceAll("\\s+", " ");
							System.out.println(data);
							hw = data.split(" ")[2];
						}
						break;
					}

				}
				log("hw is :" + hw);
			} else {
				log("ip is null");
			}
		} catch (IOException e) {
			log("Can't open/read file ARP: " + e.getMessage());
			e.printStackTrace();

		} finally {
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (hw == null || hw.length() == 0) {
			try {
				InetAddress.getByName(ip).isReachable(200);
				return getHardwareAddress(ip, (count - 1));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return hw;
	}

	private static final void log(String string) {
		System.out.println(string);
	}

	public String getWallyESCPOSPrinterBrand(final String macAddress) {
		for (WallyPrinter.WallyPrinterBrand brand : WallyPrinter.printerBrands) {
			if (macAddress != null) {
				if (macAddress.toUpperCase().contains(brand.getMac())) {
					return brand.getBrand();
				}
			}
		}

		return "Desconocido";

	}

	public WallyESCPOSPrinter getWallyESCPOSPrinter(WallyPrinter printer) {
		WallyESCPOSPrinter escposPrinter = null;
		String mac = printer.getMacAddress();
		for (WallyPrinter.WallyPrinterBrand brand : WallyPrinter.printerBrands) {
			if (mac != null && mac.toUpperCase().contains(brand.getMac())) {
				try {
					escposPrinter = (WallyESCPOSPrinter) brand.getPrinterClass().newInstance();
				} catch (Exception e) {
					e.printStackTrace();
				}
				escposPrinter.init(printer);
				break;
			}
		}

		if (printer.getIp().equalsIgnoreCase("USB")) {
			String printerName = printer.getMacAddress().toLowerCase();
			if (printerName.contains("star")) {
				escposPrinter = new WallyESCPOSPrinterSTARSP742();
				escposPrinter.init(printer);
			} else if (printerName.contains("bixolon")) {
				escposPrinter = new WallyESCPOSPrinterBIXOLON();
				escposPrinter.init(printer);
			} else if (printerName.contains("epson")) {
				escposPrinter = new WallyESCPOSPrinterEPSON();
				escposPrinter.init(printer);
			} else {
				if (escposPrinter == null) {
					escposPrinter = new WallyESCPOSPrinterEthernet();
					escposPrinter.init(printer);
				}
			}

		} else {
			if (escposPrinter == null) {
				escposPrinter = new WallyESCPOSPrinterEthernet();
				escposPrinter.init(printer);
			}
		}

		log("Clase seleccionada: " + escposPrinter.getClass().getName());
		return escposPrinter;

	}

}