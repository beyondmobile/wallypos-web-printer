package pe.beyond.wally.websocket.manager;

public class WallyWebSocketException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public WallyWebSocketException(String errorMessage){
		super(errorMessage);
	}
	
}
