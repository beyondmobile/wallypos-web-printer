package pe.beyond.wally.websocket.gui;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintStream;
import java.net.BindException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.component.LifeCycle;
import org.eclipse.jetty.util.component.LifeCycle.Listener;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import com.google.gson.Gson;

import pe.beyond.wally.external.printer.wallyprinter.WallyESCPOSPrinter;
import pe.beyond.wally.external.printer.wallyprinter.WallyPrinter;
import pe.beyond.wally.external.printer.wallyprinter.WallyPrinterTickets;
import pe.beyond.wally.external.printer.wallyprinter.ticket.TestTicket;
import pe.beyond.wally.websocket.WallyWebSocketHandler;
import pe.beyond.wally.websocket.manager.WallyBase64;
import pe.beyond.wally.websocket.manager.WallyPrinterController;
import pe.beyond.wally.websocket.manager.WallyPrinterManager;
import pe.beyond.wally.websocket.model.WallyResponse;

public class WallyWebSocketUI extends JFrame {

	private static final String SAMPLE_PRINTER_NAME = "SP742";
	private static final String SAMPLE_PRINTER_IP = "192.168.1.200";
	private static final String VERSION = "1.8.8";
	public static boolean IS_FOR_DEVELOPMENT = false;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTextArea textArea;
	static JLabel labelStatus;
	static JLabel labelIP;
	static JTextField textFieldIP;
	static JLabel jLabelResult;
	static JButton button;
	static int selectedType = 0;
	static int counter = 0;

	private void initUI() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		JPanel panel = new JPanel();

		BufferedImage myPicture = null;
		try {
			myPicture = ImageIO.read(getClass().getResource("/img/ic_launcher.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		getContentPane().setBackground(Color.WHITE);

		panel.setBackground(Color.WHITE);
		add(panel);
		panel.setLayout(new GridBagLayout());

		constraints = new GridBagConstraints();
		JLabel picLabel = new JLabel(new ImageIcon(myPicture), SwingConstants.CENTER);
		// picLabel.setHorizontalAlignment(SwingConstants.CENTER);
		// picLabel.setVerticalAlignment(SwingConstants.CENTER);
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 0;
		panel.add(picLabel, constraints);

		// titulo
		constraints = new GridBagConstraints();
		JLabel labelTitle = new JLabel("Test de Impresion", SwingConstants.CENTER);
		labelTitle.setFont(new Font("SansSerif", Font.PLAIN, 14));
		constraints.fill = GridBagConstraints.BOTH;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		panel.add(labelTitle, constraints);

		String[] comboTypes = new String[] { "IMPRESORA DE RED", "IMPRESORA USB" };
		constraints = new GridBagConstraints();
		JComboBox<String> comboBoxTypes = new JComboBox<>(comboTypes);
		comboBoxTypes.addActionListener(clickComboType);
		// add to the parent container (e.g. a JFrame):

		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 2;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		panel.add(comboBoxTypes, constraints);

		constraints = new GridBagConstraints();
		JPanel panelGroupType = new JPanel();
		panelGroupType.setBorder(BorderFactory.createLineBorder(Color.black));
		panelGroupType.setLayout(new GridBagLayout());

		////////////////////////
		constraints = new GridBagConstraints();
		labelIP = new JLabel("IP de Impresora:");
		constraints.insets = new Insets(10, 10, 10, 10);
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		panelGroupType.add(labelIP, constraints);

		constraints = new GridBagConstraints();
		textFieldIP = new JTextField(SAMPLE_PRINTER_IP, 12);
		// textFieldIP.setPlaceholder(SAMPLE_PRINTER_IP);
		textFieldIP.setToolTipText(SAMPLE_PRINTER_IP);
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		panelGroupType.add(textFieldIP, constraints);
		////////////////////////////////
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		panel.add(panelGroupType, constraints);

		constraints = new GridBagConstraints();
		button = new JButton("Probar Impresora");
		button.addActionListener(clickButtonTest);
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 4;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.gridwidth = 2;
		constraints.insets = new Insets(10, 10, 10, 10);
		panel.add(button, constraints);

		constraints = new GridBagConstraints();
		jLabelResult = new JLabel("");
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 5;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.insets = new Insets(10, 10, 10, 10);
		panel.add(jLabelResult, constraints);

		constraints = new GridBagConstraints();
		labelStatus = new JLabel("STATUS: OFF", SwingConstants.CENTER);
		labelStatus.setFont(new Font("SansSerif", Font.BOLD, 16));
		labelStatus.setForeground(Color.RED);
		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 6;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		panel.add(labelStatus, constraints);

		constraints.gridwidth = 2;
		constraints = new GridBagConstraints();
		textArea = new JTextArea(5, 40);
		textArea.setEditable(false);
		if (IS_FOR_DEVELOPMENT) {
			PrintStream printStream = new PrintStream(new CustomOutputStream(textArea));
			System.setOut(printStream);
			System.setErr(printStream);
			// creates the GUI

			constraints.insets = new Insets(10, 10, 10, 10);
			constraints.anchor = GridBagConstraints.WEST;
			constraints.gridx = 0;
			constraints.gridy = 7;
			// constraints.gridwidth = 2;
			constraints.fill = GridBagConstraints.BOTH;
			constraints.weightx = 1.0;
			constraints.weighty = 1.0;

			add(new JScrollPane(textArea), constraints);
			panel.setSize(300, 300);
			setSize(650, 600);
		} else {
			textArea.setVisible(false);
			panel.setSize(100, 100);
			setSize(500, 400);

		}
		setTitle("Wally Printer");

		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

	public WallyWebSocketUI() {
		initUI();

	}

	private ActionListener clickComboType = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent event) {
			JComboBox<String> combo = (JComboBox<String>) event.getSource();
			int selectedBook = combo.getSelectedIndex();

			if (selectedBook == 0) {
				labelIP.setText("IP de la impresora:");
				textFieldIP.setToolTipText(SAMPLE_PRINTER_IP);
				// textFieldIP.setPlaceholder(SAMPLE_PRINTER_IP);
				selectedType = 0;
			} else if (selectedBook == 1) {
				labelIP.setText("Nombre de la impresora:");
				textFieldIP.setToolTipText(SAMPLE_PRINTER_NAME);
				// textFieldIP.setPlaceholder(SAMPLE_PRINTER_NAME);
				selectedType = 1;
			}
		}
	};

	private ActionListener clickButtonTest = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {

			
			new Thread(new Runnable() {

				@Override
				public void run() {
					WallyPrinterManager manager = new WallyPrinterManager();
					String ip = textFieldIP.getText();
					jLabelResult.setText("Verificando...");
					button.setEnabled(false);

					
					WallyPrinter printer = null;
					if (selectedType == 0) {
						try {
							printer = manager.addWallyPrinter(ip);
						} catch (Exception e) {
							counter = 0;
							labelStatus.setForeground(Color.RED);
							jLabelResult.setText("ERROR: Verifique la conexion con la impresora");
							button.setEnabled(true);
							button.setText("Probar Impresora");
							e.printStackTrace();
							return;
						}
						printer.setDescription("impresora de test");
					} else {
						printer = new WallyPrinter();
						printer.setDescription("impresora de test");
						printer.setIp("USB");
						printer.setMacAddress(ip);
					}
					
					final WallyESCPOSPrinter escpos = manager.getWallyESCPOSPrinter(printer);;

					final TestTicket testTicket = new TestTicket();
					testTicket.setVersionName(VERSION);
					testTicket.setIdentifier(printer.getIp());
					testTicket.setMacAddress(printer.getMacAddress());
					testTicket.setPrinterName(printer.getDescription());
					
					countDown(10);
					try {
						runWithTimeout(new Runnable() {
							
							@Override
							public void run() {
								WallyPrinterTickets.printTestTicket(escpos, testTicket);
							}
						}, 10, TimeUnit.SECONDS);
					} catch (Exception e) {
						counter = 0;
						labelStatus.setForeground(Color.RED);
						jLabelResult.setText("ERROR: Verifique la conexion con la impresora");
						button.setEnabled(true);
						button.setText("Probar Impresora");
						e.printStackTrace();
						return;
					}
					
					
					Thread.currentThread().interrupt();
					String errorMessage = escpos.getErrorMessage();
					log("selectedType: " + selectedType);

					if (selectedType == 1) {
						String decodeData = new String(WallyBase64.decode(WallyPrinterController.validatePrinter(escpos, printer)));
						System.out.println("request Decoded: " + decodeData);
						WallyResponse response = new Gson().fromJson(decodeData, WallyResponse.class);
						errorMessage = response.getErrorMessage();
					}

					if (errorMessage != null && errorMessage.length() > 0) {
						log("Error Message :" + errorMessage);
						labelStatus.setForeground(Color.RED);
						jLabelResult.setText("ERROR:" + errorMessage);
					} else {
						labelStatus.setForeground(Color.BLUE);
						jLabelResult.setText("Se mandó a imprimir");
					}
					counter = 0;
					button.setEnabled(true);
					button.setText("Probar Impresora");
				}
			}).start();

		}
	};

	private static void runWebSocket() {
		System.out.println("RUN WEB SOCKET");
		Server server = new Server(9999);
		WebSocketHandler wsHandler = new WebSocketHandler() {
			@Override
			public void configure(WebSocketServletFactory factory) {
				factory.register(WallyWebSocketHandler.class);
			}
		};
		server.setHandler(wsHandler);
		try {

			server.addLifeCycleListener(new Listener() {

				@Override
				public void lifeCycleStopping(LifeCycle arg0) {
					// TODO Auto-generated method stub

				}

				@Override
				public void lifeCycleStopped(LifeCycle arg0) {
					// TODO Auto-generated method stub
					labelStatus.setText("STATUS: OFF");
				}

				@Override
				public void lifeCycleStarting(LifeCycle arg0) {
					// TODO Auto-generated method stub
					log("Iniciando Wally Printer...");
				}

				@Override
				public void lifeCycleStarted(LifeCycle arg0) {
					// TODO Auto-generated method stub
					log("Wally Printer está inicializado");
					labelStatus.setText("STATUS: ON");
					labelStatus.setForeground(Color.BLACK);
				}

				@Override
				public void lifeCycleFailure(LifeCycle arg0, Throwable arg1) {
					// TODO Auto-generated method stub
				}
			});

			server.start();
			server.join();

		} catch (BindException e) {
			log("Wally Printer ya está corriendo, por favor cierre esta aplicación");
			System.exit(0);
			e.printStackTrace();
		} catch (Exception e) {
			log(e.getMessage());
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				WallyWebSocketUI app = new WallyWebSocketUI();
				app.setVisible(true);
				app.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
				app.addWindowListener(new WindowListener() {

					@Override
					public void windowOpened(WindowEvent e) {
						// TODO Auto-generated method stub
						new Thread(new Runnable() {

							@Override
							public void run() {
								runWebSocket();
							}
						}).start();
					}

					@Override
					public void windowIconified(WindowEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void windowDeiconified(WindowEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void windowDeactivated(WindowEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void windowClosing(WindowEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void windowClosed(WindowEvent e) {
						// TODO Auto-generated method stub

					}

					@Override
					public void windowActivated(WindowEvent e) {

					}
				});
			}
		});

	}

	private static final void log(String message) {
		System.out.println(message);
	}

	public static void runWithTimeout(final Runnable runnable, long timeout, TimeUnit timeUnit) throws Exception {
		runWithTimeout(new Callable<Object>() {
			@Override
			public Object call() throws Exception {
				runnable.run();
				return null;
			}
		}, timeout, timeUnit);
	}

	public static <T> T runWithTimeout(Callable<T> callable, long timeout, TimeUnit timeUnit) throws Exception {
		final ExecutorService executor = Executors.newSingleThreadExecutor();
		final Future<T> future = executor.submit(callable);
		executor.shutdown(); // This does not cancel the already-scheduled task.
		try {
			return future.get(timeout, timeUnit);
		} catch (TimeoutException e) {
			// remove this if you do not want to cancel the job in progress
			// or set the argument to 'false' if you do not want to interrupt the thread
			future.cancel(true);
			throw e;
		} catch (ExecutionException e) {
			// unwrap the root cause
			Throwable t = e.getCause();
			if (t instanceof Error) {
				throw (Error) t;
			} else if (t instanceof Exception) {
				throw (Exception) t;
			} else {
				throw new IllegalStateException(t);
			}
		}
	}
	
	public static void countDown(int count) {
		counter = count;
		new Thread() {
			public void run() {
				
				while (counter >= 0) {
					if(counter <= 0) {
						button.setText("Probar Impresora");
						break;
					}
					
					button.setText("Probar Impresora " + (counter--));
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
}
