package pe.beyond.wally.websocket.manager;

import java.awt.print.PrinterJob;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.SimpleDoc;
import javax.xml.bind.DatatypeConverter;

import com.google.gson.Gson;

import pe.beyond.wally.external.mail.GMailSender;
import pe.beyond.wally.external.printer.wallyprinter.WallyESCPOSPrinter;
import pe.beyond.wally.external.printer.wallyprinter.WallyPrinter;
import pe.beyond.wally.external.printer.wallyprinter.WallyPrinterTickets;
import pe.beyond.wally.external.printer.wallyprinter.ticket.CashRegisterTurnReportTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.CashRegisterTurnTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.CommandTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.CreditNoteReportTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.DeliveryTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.MiniTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.PreAccountTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.ProductReportTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.SaleReportTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.TestTicket;
import pe.beyond.wally.external.printer.wallyprinter.ticket.VoucherTicket;
import pe.beyond.wally.websocket.gui.WallyWebSocketUI;
import pe.beyond.wally.websocket.model.WallyRequest;
import pe.beyond.wally.websocket.model.WallyResponse;

public class WallyPrinterController {
	
	public static final String COMMAND_PRINT_VOUCHER_TICKET = "printVoucherTicket";
	public static final String COMMAND_PRINT_TEST_TICKET = "printTestTicket";
	public static final String COMMAND_PRINT_MINI_TICKET= "printMiniTicket";
	
	public static final String COMMAND_PRINT_COMMAND_TICKET = "printCommandTicket";
	public static final String COMMAND_PRINT_PREACCOUNT_TICKET = "printPreAccountTicket";
	public static final String COMMAND_PRINT_CASH_REGISTER_TURN_REPORT_TICKET = "printCashRegisterTurnReportTicket";
	
	
	public static final String COMMAND_PRINT_CREDIT_NOTE_REPORT= "printCreditNoteReport";
	
	
	public static final String COMMAND_PRINT_CASH_REGISTER_TURN_TICKET = "printCashRegisterTurnTicket";
	public static final String COMMAND_PRINT_PRODUCT_REPORT_TICKET = "printProductReportTicket";
	
	
	@Deprecated
	public static final String COMMAND_PRINT_SALE_REPORT_TICKET = "printSaleReportTicket";
	
	public static final String COMMAND_FIND_PRINTERS = "findPrinters"; //
	public static final String COMMAND_PRINT_DELIVERY_TICKET = "printDeliveryTicket";
	public static final String COMMAND_GET_MAC_ADDRESS = "findMACAddress";
	public static final String COMMAND_GET_PRINTER_STATUS= "getPrinterStatus";
	public static final String COMMAND_OPEN_CASH_DRAWER = "openCashDrawer";
	
	//decode   String encodedHelloBytes = new String(DatatypeConverter.parseBase64Binary(data), StandardCharsets.UTF_8) ;
	//encode   String DatatypeConverter.printBase64Binary(sha1)
	
	public String validateCommand (String dataBase64) throws WallyWebSocketException{
		//String decodeData = new String(DatatypeConverter.parseBase64Binary(dataBase64), StandardCharsets.UTF_8) ;
		String dataDecoded = null;
		String decodeData = null;
		try {
		decodeData = new String(WallyBase64.decode(dataBase64));
		System.out.println("request Decoded: " + decodeData);
		WallyRequest request = new Gson().fromJson(decodeData, WallyRequest.class);
		String dataEncoded = request.getData();
		
		if (dataEncoded != null){
			dataDecoded = new String(DatatypeConverter.parseBase64Binary(dataEncoded), StandardCharsets.UTF_8) ;
			System.out.println("dataDecoded: " + dataDecoded);
		}
		//WallyRequest request = new WallyRequest();
		//request.setData("");
		//request.setCommand("findPrinters");
		
		//WallyPrinter printer = new WallyPrinter();
		//printer.setIp("192.168.1.150");
		//printer.setMacAddress("33:33:33:33:33:33");
		WallyPrinterManager manager = new WallyPrinterManager();
		WallyPrinter printer = request.getPrinter();
		WallyESCPOSPrinter escpos =  null;
		if (printer != null)
			escpos = manager.getWallyESCPOSPrinter(printer);
		
			Gson gson = new Gson();
			if (request.getCommand().equalsIgnoreCase(COMMAND_FIND_PRINTERS)) {
				List<WallyPrinter> printers = manager.getPrinters();
				String response = new Gson().toJson(printers );
				return getResponse(response,false);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_TEST_TICKET)) {
				TestTicket object = gson.fromJson(dataDecoded, TestTicket.class);
				escpos = WallyPrinterTickets.printTestTicket(escpos, object);
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_CASH_REGISTER_TURN_TICKET)) {
				CashRegisterTurnTicket object = gson.fromJson(dataDecoded, CashRegisterTurnTicket.class);
				escpos = (WallyPrinterTickets.printCashRegisterTurnTicket(escpos, object));
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_CASH_REGISTER_TURN_REPORT_TICKET)) {
				CashRegisterTurnReportTicket object = gson.fromJson(dataDecoded, CashRegisterTurnReportTicket.class);
				escpos = (WallyPrinterTickets.printCashRegisterTurnReportTicket(escpos, object));
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_COMMAND_TICKET)) {
				CommandTicket object = gson.fromJson(dataDecoded, CommandTicket.class);
				escpos = (WallyPrinterTickets.printCommandTicket(escpos, object));
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_DELIVERY_TICKET)) {
				DeliveryTicket object = gson.fromJson(dataDecoded, DeliveryTicket.class);
				escpos = (WallyPrinterTickets.printDeliveryTicket(escpos, object));
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_MINI_TICKET)) {
				MiniTicket object = gson.fromJson(dataDecoded, MiniTicket.class);
				escpos = (WallyPrinterTickets.printMiniTicket(escpos, object));
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_PREACCOUNT_TICKET)) {
				PreAccountTicket object = gson.fromJson(dataDecoded, PreAccountTicket.class);
				escpos = (WallyPrinterTickets.printPreAccountTicket(escpos, object));
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_SALE_REPORT_TICKET)) {
				SaleReportTicket object = gson.fromJson(dataDecoded, SaleReportTicket.class);
				escpos = (WallyPrinterTickets.printSaleReportTicket(escpos, object));
				return validatePrinter(escpos, printer);
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_PRINT_VOUCHER_TICKET)) {
				VoucherTicket object = gson.fromJson(dataDecoded, VoucherTicket.class);
				escpos = (WallyPrinterTickets.printVoucherTicket(escpos, object));
				return validatePrinter(escpos, printer);
				
			} else if(request.getCommand().equalsIgnoreCase(COMMAND_PRINT_CREDIT_NOTE_REPORT)){
				CreditNoteReportTicket object = gson.fromJson(dataDecoded, CreditNoteReportTicket.class);
				escpos = (WallyPrinterTickets.printCreditNoteTicket(escpos, object));
				return validatePrinter(escpos, printer);
				
			}else if(request.getCommand().equalsIgnoreCase(COMMAND_PRINT_PRODUCT_REPORT_TICKET)){
				ProductReportTicket object = gson.fromJson(dataDecoded, ProductReportTicket.class);
				escpos = (WallyPrinterTickets.printProductReportTicket(escpos, object));
				return validatePrinter(escpos, printer);
				
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_GET_PRINTER_STATUS)) {
				 String status = null;
				//Map<String, String> map = new HashMap<String, String>();
				//int statusCode = 0;
				if(!printer.getIp().equalsIgnoreCase("USB")){
					status = escpos.getPrinterStatus(true);
					if (status != null && status.length() > 0){
						//statusCode = -1;
						
					}
				} 
				//map.put("status", ""+statusCode);
				//map.put("errorMessage", ""+status);
				//String response = new Gson().toJson(map );
				return getResponse(status,true);
					
					 
				
			} else if (request.getCommand().equalsIgnoreCase(COMMAND_GET_MAC_ADDRESS)) {
				Map<String, String> map = new HashMap<String, String>();
				
				String macAddress = "";
		
				macAddress = getMacAddress();
				System.out.println(macAddress);

				map.put("macAddress", macAddress);
				
				String response = new Gson().toJson(map );
				return getResponse(response,false);
			}else if(request.getCommand().equalsIgnoreCase(COMMAND_OPEN_CASH_DRAWER)){
				escpos = WallyPrinterTickets.openCashDrawer(escpos);
				return validatePrinter(escpos, printer);
				
			}

		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			String stringErrors = errors.toString();
			System.out.println(stringErrors);
			if (!WallyWebSocketUI.IS_FOR_DEVELOPMENT)
				sendErrorMail(dataBase64, stringErrors);
			throw new WallyWebSocketException("Datos incorrectos : " + e.getMessage());
			
		}
		
		throw new WallyWebSocketException("Comando incorrecto");
		
	}
	
	
	private static String getMacAddress(){
		try {
		 Socket socket= new Socket();
		    SocketAddress endpoint= new InetSocketAddress("www.google.com", 80);
		    socket.connect(endpoint);
		    InetAddress localAddress = socket.getLocalAddress();
		    socket.close();
		    System.out.println(localAddress.getHostAddress());
		    NetworkInterface ni = NetworkInterface.getByInetAddress(localAddress);
		    byte[] mac = ni.getHardwareAddress();
		    if (mac != null){
		        return bytesToHex(mac);
		    }
		    else{
		        System.out.println("Address doesn't exist or is not accessible.");
		        return null;
		    }
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	
	private static  String bytesToHex(byte[] data){
		StringBuilder sb = new StringBuilder();
		if (data != null){
			for (int i = 0; i < data.length; i++) {
				sb.append(String.format("%02X%s", data[i],(i < data.length - 1) ? ":" : ""));
			}
		}
		
		String macAddress = sb.toString();
		return macAddress;
	}
	
	
	private static String getResponse(String data, boolean isFromError){
		WallyResponse response = new WallyResponse();
		response.setData(data);
		if (isFromError && data!= null && data.length() > 0){
			response.setStatusCode(500);
		} else if (!isFromError){
			response.setStatusCode(0);
		} else{
			response.setStatusCode(0);
		}
		String json = new Gson().toJson(response);
		System.out.println("RESPONSE: " + json);
		return WallyBase64.encode(json.getBytes());
	}
	
	
	public static final String SUPPORTED_USB_PRINTER = "SP742";
	
	public static String validatePrinter(WallyESCPOSPrinter data, WallyPrinter printer) {
		if (printer.getIp().equalsIgnoreCase("USB")) {
			final String ERROR_USB_PRINTER ="Verifique la conexión/driver de la Impresora USB : ";
			PrintService[] services = PrinterJob.lookupPrintServices();
			try {
				DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
				PrintService pservice = null;
				
				//PrintService defaultPrintService = PrintServiceLookup.lookupDefaultPrintService();
				/*
				if (defaultPrintService != null){
					pservice = defaultPrintService;
				}*/
				
				
				System.out.println("printer.getMacAddress():" + printer.getMacAddress());
				
				//if (pservice != null){
					// Retrieve a print service from the array
					for (int index = 0; index < services.length; index++) {
						System.out.println("IMPRESORA :"
								+ services[index].getName()+"::");
						if (services[index].getName().toLowerCase().contains(printer.getMacAddress().toLowerCase())) {
							pservice = services[index];
							break;
						}
					}

					if (pservice == null) {
						return getResponse(ERROR_USB_PRINTER + printer.getMacAddress(),true);
					}
					
					for (PrintService printService : services) {
					   
					    if (printService.getName().contains(printer.getMacAddress())){
					    	 System.out.println("Found print service: " + printService);
					    	DocFlavor[] flavors = printService.getSupportedDocFlavors();
					    	  for (DocFlavor flavorX : flavors) {
						        	System.out.println("flavor getMimeType:" + flavorX.getMimeType());
					    	  }

						 
					    }

					}
				//}
				
				
				System.out.println("PRINTER SELECTED: " + pservice);
				

				System.out.println("PRINTER SELECTED: " + pservice.getName());
				System.out.println("data.getCommands():" + data.getCommands().size());
				DocPrintJob pjob = pservice.createPrintJob();
				Doc doc = new SimpleDoc(data.getCommands().getByteArray(),
						flavor, null);

				pjob.print(doc, null); 
				
				return getResponse("",false);
				
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("ERROR: " + (e.getMessage()));
				return getResponse(ERROR_USB_PRINTER + printer.getMacAddress(),true);
			}

		} else {
			return getResponse(data.getErrorMessage(),true);
		}
	}
	
	
	public void sendErrorMail(final String data, final String errorMessage){
		final GMailSender sender = new GMailSender();
		try {
	
			final StringBuilder body = new StringBuilder();
			body.append("DATA:\n").append(data).append("\n\n").append("ERROR:\n").append(errorMessage);

			new Thread(new Runnable() {
				@Override
				public void run() {
					String target  = "dvelasquez@beyond.pe,stamariz@beyond.pe, mvela@beyond.pe, azavaleta@beyond.pe";
					try {
						sender.sendMail(
								"[WALLY WEB ERROR EN IMPRESION] >> Fecha " + new Date().toString(),
								body.toString(),
								"wally.error@beyond.pe",
								target);


					}catch (Exception e){
						System.out.println("EXCEPCION : " + e.getMessage());
						e.printStackTrace();
						//saleOrderOffline.setSentErrorMail(false);
						//SaleOrderOffline.saveInTx(saleOrderOffline);
					}
				}
			}).start();





		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
