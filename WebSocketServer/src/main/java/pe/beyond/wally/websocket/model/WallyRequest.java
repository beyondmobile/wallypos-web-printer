package pe.beyond.wally.websocket.model;

import pe.beyond.wally.external.printer.wallyprinter.WallyPrinter;


public class WallyRequest {
	
	private String command;
	private String data;
	private WallyPrinter printer;
	
	
	
	public WallyPrinter getPrinter() {
		return printer;
	}
	public void setPrinter(WallyPrinter printer) {
		this.printer = printer;
	}
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	
	
}
