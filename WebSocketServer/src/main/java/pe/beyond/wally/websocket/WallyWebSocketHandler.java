package pe.beyond.wally.websocket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import pe.beyond.wally.external.printer.wallyprinter.WallyESCPOSPrinter;
import pe.beyond.wally.external.printer.wallyprinter.WallyPrinter;
import pe.beyond.wally.external.printer.wallyprinter.WallyPrinterTickets;
import pe.beyond.wally.external.printer.wallyprinter.escpos.WallyESCPOSPrinterEthernet;
import pe.beyond.wally.external.printer.wallyprinter.ticket.TestTicket;
import pe.beyond.wally.websocket.manager.WallyBase64;
import pe.beyond.wally.websocket.manager.WallyPrinterController;
import pe.beyond.wally.websocket.model.WallyResponse;

import com.google.gson.Gson;

@WebSocket
public class WallyWebSocketHandler {
	
	Session mSession;
	WallyPrinterController controller;

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
    	mSession = null;
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
    	mSession = session;
    	controller = new WallyPrinterController();
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        try {
           // session.getRemote().sendString("Hello Webbrowser");
        	
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    

    @OnWebSocketMessage
    public void onMessage(String message) {
       log("Message: " + message);
        try {
        	String responseData = controller.validateCommand(message);
        	log(responseData);
        	if (responseData == null)
        		responseData = "";
        	
        	/*
        	WallyResponse response = new WallyResponse();
			response.setStatusCode(0);
			String json = new Gson().toJson(response);*/
        	
        	mSession.getRemote().sendString(responseData);
        	//testPrinter();
		} catch (Exception e) {
			log(e.getMessage());
			try {
				WallyResponse response = new WallyResponse();
				response.setErrorMessage(e.getMessage());
				response.setStatusCode(-1);
				String json = new Gson().toJson(response);
				mSession.getRemote().sendString(WallyBase64.encode(json.getBytes()));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
    }
    
    
    private void testPrinter(){
    	WallyPrinter printer = new WallyPrinter();
    	printer.setDescription("bixolon");
    	printer.setIp("192.168.1.150");
    	printer.setMacAddress("33:33:44:55:66");
    	
    	WallyESCPOSPrinter escpos = new WallyESCPOSPrinterEthernet();
    	escpos.init(printer);
    	TestTicket testTicket = new TestTicket();
    	testTicket.setIdentifier(printer.getIp());
    	testTicket.setMacAddress(printer.getMacAddress());
    	testTicket.setPrinterName(printer.getDescription());
    	WallyPrinterTickets.printTestTicket(escpos, testTicket);
    }
    
    private String testGetPrinters(){
    	List<WallyPrinter> printers = new ArrayList<WallyPrinter>();
    	WallyPrinter printer = new WallyPrinter();
    	printer.setDescription("bixolon0");
    	printer.setIp("192.168.1.150");
    	printer.setMacAddress("33:33:44:55:66");
    	printers.add(printer);
    	
    	printer = new WallyPrinter();
    	printer.setDescription("bixolon1");
    	printer.setIp("192.168.1.150");
    	printer.setMacAddress("33:33:44:55:66");
    	printers.add(printer);
    	
    	printer = new WallyPrinter();
    	printer.setDescription("bixolon2");
    	printer.setIp("192.168.1.150");
    	printer.setMacAddress("33:33:44:55:66");
    	printers.add(printer);
    	
    	String json = new Gson().toJson(printers );
    	System.out.println("gson: " + json);
    	return json;
    	
    }
  
    
	private static final void log(String message){
		System.out.println(message);
	}
    
    
}